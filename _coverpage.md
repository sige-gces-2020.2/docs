<h1 style="font-weight: 400;"> SIGE </h1>

<h2 style="font-weight: normal;">
    Sistema de Gestão Energética da Universidade de Brasília
</h2>

<h3 style="font-weight: normal;">
    Monitoramento contínuo e em tempo real da infraestrutura elétrica
</h3>

[Gitlab](https://gitlab.com/groups/lappis-unb/projects/SMI)
[Documentação](#sige-sistema-de-gestão-energética)